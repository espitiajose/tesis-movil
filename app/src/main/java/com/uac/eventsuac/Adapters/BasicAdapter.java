package com.uac.eventsuac.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.christophesmet.android.views.maskableframelayout.MaskableFrameLayout;
import com.uac.eventsuac.Models.Event;
import com.uac.eventsuac.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BasicAdapter extends RecyclerView.Adapter<BasicAdapter.PromotionViewHolder> {

    private Context context;
    private ArrayList<Event> items;
    private final OnItemClickListener listener;
    private int selectedPos = RecyclerView.NO_POSITION;

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public class PromotionViewHolder extends RecyclerView.ViewHolder {

        private MaskableFrameLayout parent;
        private ImageView mImage;
        private TextView mTitleText, mDateText, mWorshopText;
        private Pair[] pairs = new Pair[4];

        PromotionViewHolder(View itemView) {
            super(itemView);
            parent = (MaskableFrameLayout) itemView;
            mImage = itemView.findViewById(R.id.imageProm);
            mTitleText = itemView.findViewById(R.id.titleText);
            mDateText = itemView.findViewById(R.id.dateText);
            mWorshopText = itemView.findViewById(R.id.workshopText);
            pairs[0] = new Pair<View, String>(mImage, "imageEffect");
            pairs[1] = new Pair<View, String>(mTitleText, "titleEffect");
            pairs[2] = new Pair<View, String>(mDateText, "dateEffect");
            pairs[3] = new Pair<View, String>(mWorshopText, "workshopEffect");
        }

        void setSelectedPosition()
        {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }

        void bindNews(final int pos, final OnItemClickListener listener) {
            Event item = items.get(pos);
            mTitleText.setText(item.getName());
            if(item.getStartDate() != null) mDateText.setText(getDateFormat(item.getStartDate()));
            if(item.getImage()!=null)
            Glide.with(context).load(item.getImage())
                    .error(context.getResources().getDrawable(R.drawable.background_notimage))
                    .placeholder(context.getResources().getDrawable(R.drawable.background_notimage))
                    .apply(RequestOptions.centerCropTransform()).into(mImage);
            if(item.getProgram()!= null) mWorshopText.setText(item.getProgram().getName());
            else mWorshopText.setText("Programa");
            parent.setOnClickListener(v -> {
                listener.onItemClick(item, pos, pairs, mImage);
                setSelectedPosition();
            });
        }

    }

    private String getDateFormat(Date date){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(date);
    }

    public BasicAdapter(Context context, ArrayList<Event> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Event item, int pos, Pair[] pairs, ImageView imageView);
    }


    @Override
    public PromotionViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_promotion, viewGroup, false);
        return new PromotionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PromotionViewHolder viewHolder, int pos) {

        viewHolder.itemView.setSelected(selectedPos == pos);
        viewHolder.bindNews(pos, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
