package com.uac.eventsuac.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.uac.eventsuac.R;

import java.util.ArrayList;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.FilterViewHolder> {

    private Context context;
    private ArrayList<String> items;
    private final OnItemClickListener listener;
    private int selectedPos = RecyclerView.NO_POSITION;

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {

        private TextView parent;

        FilterViewHolder(View itemView) {
            super(itemView);
            parent = (TextView) itemView;
        }

        void setSelectedPosition()
        {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }

        void bindNews(final int pos, final OnItemClickListener listener) {
            String item = items.get(pos);
            parent.setText(item);
            parent.setOnClickListener(v -> {
                listener.onItemClick(item, pos);
                setSelectedPosition();
            });
        }

    }

    public FiltersAdapter(Context context, ArrayList<String> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(String item, int pos);
    }


    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView;
        itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_filter, viewGroup, false);
        return new FilterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FilterViewHolder viewHolder, int pos) {

        viewHolder.itemView.setSelected(selectedPos == pos);
        viewHolder.bindNews(pos, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
