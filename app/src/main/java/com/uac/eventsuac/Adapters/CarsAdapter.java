package com.uac.eventsuac.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.uac.eventsuac.Activities.ProgramDeleteActivity;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;
import com.uac.eventsuac.customViews.NotifyAlert;

import java.io.Serializable;
import java.util.ArrayList;

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.CarViewHolder> {

    private Context context;
    private ArrayList<Program> items;
    private final OnItemClickListener listener;
    private int selectedPos = RecyclerView.NO_POSITION;

    public void setSelectedPos(int selectedPos) {
        this.selectedPos = selectedPos;
    }

    public class CarViewHolder extends RecyclerView.ViewHolder {

        private FrameLayout parent;
        private TextView mNameText, mDataCar;
        private ImageButton deleteBtn;
        private CheckBox checkCar;

        CarViewHolder(View itemView) {
            super(itemView);
            parent = (FrameLayout) itemView;
            mNameText = itemView.findViewById(R.id.nameCar);
            mDataCar = itemView.findViewById(R.id.dataCar);
            deleteBtn = itemView.findViewById(R.id.deleteReplyBtn);
            checkCar = itemView.findViewById(R.id.checkCar);
        }

        void setSelectedPosition()
        {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }

        void bindNews(final int pos, final OnItemClickListener listener) {
            Program item = items.get(pos);
            if(getItemCount() > 1) checkCar.setChecked(pos == selectedPos);
            mNameText.setText(item.getName());
            mDataCar.setText("");
            if(getItemCount() <= 1) deleteBtn.setVisibility(View.GONE);
            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NotifyAlert dialogChronometer = new NotifyAlert(context, true, mNameText.getText().toString(), mDataCar.getText().toString(),
                            null, "¿Estas seguro de querer eliminar este programa?",
                            (dialog, res) -> {
                                if(res){
                                    Intent intent = new Intent(context, ProgramDeleteActivity.class);
                                    intent.putExtra("program", (Serializable) item);
                                    intent.putExtra("pos", pos+1);
                                    context.startActivity(intent);
                                }
                            });
                    dialogChronometer.show();
                }
            });
            parent.setOnClickListener(v -> {
                listener.onItemClick(item, pos);
                setSelectedPosition();
            });
        }

    }

    private void removeAt(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, items.size());
    }

    public CarsAdapter(Context context, ArrayList<Program> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Program item, int pos);
    }


    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_selec_car, viewGroup, false);
        return new CarViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CarViewHolder viewHolder, int pos) {
        viewHolder.itemView.setSelected(selectedPos == pos);
        viewHolder.bindNews(pos, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
