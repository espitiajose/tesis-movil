package com.uac.eventsuac.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.uac.eventsuac.R;

import java.text.NumberFormat;
import java.util.Objects;

public class ProfileFragment extends Fragment {

    private TextView mPersonName, mDataVehicle;
    private EditText editFName, editLName, editEmail, editPhone, editAddress;
    private ImageView imgProfile;
    private String dataVehicle;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        mPersonName = v.findViewById(R.id.personName);
        mDataVehicle = v.findViewById(R.id.dataVehicle);
        editFName = v.findViewById(R.id.editFirstName);
        editLName = v.findViewById(R.id.editLastName);
        editEmail = v.findViewById(R.id.editEmail);
        editPhone = v.findViewById(R.id.editPhone);
        editAddress = v.findViewById(R.id.editAddress);
        imgProfile = v.findViewById(R.id.imgprofile);

        Bundle args = getArguments();
        if(args!=null){
            dataVehicle = args.getString("dataProgram");
            mDataVehicle.setText(dataVehicle);
        }


        v.findViewById(R.id.updateProfileBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });
        loadData();
        return v;
    }

    void loadData(){
        ParseUser user = ParseUser.getCurrentUser();
        editFName.setText(user.getString("firstName"));
        editLName.setText(user.getString("lastName"));
        editEmail.setText(user.getString("email"));
        editPhone.setText(String.valueOf(user.getNumber("phone")));
        editAddress.setText(user.getString("address"));
        mPersonName.setText(user.getString("firstName") + " "+user.getString("lastName"));
        ParseFile parseImg = user.getParseFile("image");
        if(parseImg != null)
            Glide.with(Objects.requireNonNull(getContext())).load(parseImg.getUrl())
                    .placeholder(getResources().getDrawable(R.drawable.profile))
                    .error(getResources().getDrawable(R.drawable.profile))
                    .apply(RequestOptions.circleCropTransform()).into(imgProfile);
    }

    void updateProfile() {
        if(verifyForm()){
            try{
                ParseUser user = ParseUser.getCurrentUser();
                user.put("firstName", editFName.getText().toString());
                user.put("lastName", editLName.getText().toString());
                user.put("address", editAddress.getText().toString());
                user.put("email", editEmail.getText().toString());
                user.put("phone", Objects.requireNonNull(NumberFormat.getInstance().parse(editPhone.getText().toString())));
                user.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e==null){
                            Toast.makeText(getContext(), "Usuario actualizado", Toast.LENGTH_SHORT).show();
                        }else
                            e.printStackTrace();
                    }
                });
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
        }
    }

    boolean verifyForm(){
        if(editFName.getText().toString().isEmpty()){
            editFName.setError("Campo requerido");
            return false;
        }
        if(editLName.getText().toString().isEmpty()){
            editLName.setError("Campo requerido");
            return false;
        }
        if(editEmail.getText().toString().isEmpty()){
            editEmail.setError("Campo requerido");
            return false;
        }
        if(editPhone.getText().toString().isEmpty()){
            editPhone.setError("Campo requerido");
            return false;
        }
        if(editAddress.getText().toString().isEmpty()){
            editAddress.setError("Campo requerido");
            return false;
        }

        return true;
    }

}
