package com.uac.eventsuac.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.uac.eventsuac.Activities.DescriptionActivity;
import com.uac.eventsuac.Adapters.BasicAdapter;
import com.uac.eventsuac.Adapters.FiltersAdapter;
import com.uac.eventsuac.Models.Event;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class EventsFragment extends Fragment {


    private RecyclerView filterList, promList;
    private EditText mSearchEdit;
    private TextView mCountText;
    private ArrayList<Event> news;
    private Handler mHandler = new Handler();
    private ArrayList<String> filters = new ArrayList<>();

    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_events, container, false);
        filterList = v.findViewById(R.id.listfilters);
        promList = v.findViewById(R.id.list);
        mSearchEdit = v.findViewById(R.id.searchEdit);
        mCountText = v.findViewById(R.id.countText);

        mSearchEdit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                mHandler.removeCallbacksAndMessages(null);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        searchNews(mSearchEdit.getText().toString());
                        if(keyCode == KeyEvent.KEYCODE_DEL)
                            searchNews(null);
                    }
                }, 300);
                return false;
            }
        });

        getEvents();
        return v;
    }

    private void getEvents(){
        ParseQuery<ParseObject> queryEvents = ParseQuery.getQuery("Event");
        queryEvents.whereGreaterThan("end_date", new Date());
        queryEvents.include("program");
        queryEvents.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    news = new ArrayList<>();
                    filters = new ArrayList<>();
                    filters.add("Todos");
                    for (int i = 0; i < objects.size(); i++) {
                        ParseObject p = objects.get(i);
                        Event event = new Event(p.getObjectId(),p.getString("place"), p.getDate("start_date"), p.getDate("end_date"),
                                p.getString("name"), p.getBoolean("approved"), p.getString("description"));
                        if(p.getParseFile("image")!= null){
                            event.setImage(Objects.requireNonNull(p.getParseFile("image")).getUrl());
                        }
                        if(p.getParseObject("program")!=null){
                            event.setProgram(new Program(p.getParseObject("program").getString("name")));
                        }
                        news.add(event);
                        if(!isExistTag(event.getProgram().getName())){
                            filters.add(event.getProgram().getName());
                        }
                        addFilters(filters);
                    }
                    confNews(news);
                    FiltersAdapter adapterFilter = new FiltersAdapter(getContext(), filters, (item, pos) -> {
                        if(pos != 0){
                            filterProm(item);
                        }else{
                            searchNews(null);
                        }
                    });
                    filterList.setAdapter(adapterFilter);
                }
            }
        });
    }

    private void confNews(ArrayList<Event> promotions){
        mCountText.setText(promotions.size()+" Eventos");
        BasicAdapter adapter = new BasicAdapter(getContext(),promotions, (item, pos, pairs, image)->{
            Intent intent = new Intent(getActivity(), DescriptionActivity.class);
            intent.putExtra("basic", (Serializable) item);
            intent.putExtra("image", convert(image));
            intent.putExtra("title", "Eventos");
            String date = getDateFormat(item.getStartDate());
            date += item.getEndDate() != null ? " - "+getDateFormat(item.getEndDate()) : "";
            intent.putExtra("date",  date);
            startActivity(intent);
        });
        promList.setAdapter(adapter);
    }

    private String getDateFormat(Date date){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(date);
    }

    private byte[] convert(ImageView imageView){
        try{
            if(imageView.getDrawable() != null){
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                return baos.toByteArray();
            }else
                return null;
        }catch (Exception e){
            return null;
        }
    }

    private void filterProm(String tag){
        ArrayList<Event> promotionsTag = new ArrayList<>();
        for (Event p: news) {
            if(isExistTag(tag)) promotionsTag.add(p);
        }
        confNews(promotionsTag);
    }

    private void addFilters(ArrayList<String> filtersNew){
        if(filtersNew != null)
            for (String name: filtersNew) {
                if(!name.equals("") && !isExistTag(name)) filters.add(name);
            }
    }

    private boolean isExistTag(String name){
        for (String nameTag: filters) {
            if(nameTag.equals(name)) return true;
        }
        return false;
    }

    private void searchNews(String search){
        if(search!=null){
            ArrayList<Event> newssearch = new ArrayList<>();
            for (Event prom: news) {
                if(prom.getName().toLowerCase().contains(search.toLowerCase()))
                    newssearch.add(prom);
            }
            confNews(newssearch);
        }else{
            confNews(news);
        }

    }
}



