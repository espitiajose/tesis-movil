package com.uac.eventsuac.Fragments.newUser;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import com.google.android.material.textfield.TextInputEditText;
import com.parse.ParseUser;
import com.uac.eventsuac.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewUserThreeFragment extends Fragment {

    private TextInputEditText mEmailEdit, mPasswordEdit, mPasswordRepeatEdit;

    public NewUserThreeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_new_user_3, container, false);
        mEmailEdit =v.findViewById(R.id.emailEdit);
        mPasswordEdit =v.findViewById(R.id.passwordEdit);
        mPasswordRepeatEdit =v.findViewById(R.id.passwrodConfirmEdit);
        return v;
    }

    public ParseUser getForm(ParseUser objectUser){
        if(inputChecks()){
            objectUser.put("username", mEmailEdit.getText().toString());
            objectUser.put("email", mEmailEdit.getText().toString());
            objectUser.put("password", mPasswordEdit.getText().toString());
            return objectUser;
        }else{
            return null;
        }
    }


    private boolean inputChecks() {
        if (mEmailEdit.getText().toString().isEmpty()) {
            mEmailEdit.setError("Campo requerido");
            return false;
        }
        if (mPasswordEdit.getText().toString().isEmpty()) {
            mPasswordEdit.setError("Campo requerido");
            return false;
        }
        if (mPasswordEdit.getText().toString().length() < 5) {
            mPasswordEdit.setError("La contraseña es muy débil");
            return false;
        }
        if (mPasswordRepeatEdit.getText().toString().isEmpty()) {
            mPasswordRepeatEdit.setError("Campo requerido");
            return false;
        }
        if (!mPasswordRepeatEdit.getText().toString().equals(mPasswordEdit.getText().toString())) {
            mPasswordRepeatEdit.setError("Las contraseñas deben ser iguales");
            return false;
        }
        return true;
    }

}
