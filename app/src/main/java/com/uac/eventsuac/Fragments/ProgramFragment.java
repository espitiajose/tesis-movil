package com.uac.eventsuac.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.uac.eventsuac.Activities.DescriptionActivity;
import com.uac.eventsuac.Adapters.BasicAdapter;
import com.uac.eventsuac.Adapters.BasicAdapterPrograms;
import com.uac.eventsuac.Adapters.FiltersAdapter;
import com.uac.eventsuac.Models.Event;
import com.uac.eventsuac.Models.Faculty;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProgramFragment extends Fragment {

    private RecyclerView filterList, promList;
    private EditText mSearchEdit;
    private TextView mCountText;
    private ArrayList<Program> news;
    private Handler mHandler = new Handler();
    private ArrayList<String> filters = new ArrayList<>();

    public ProgramFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_program, container, false);
        filterList = v.findViewById(R.id.listfilters);
        promList = v.findViewById(R.id.list);
        mSearchEdit = v.findViewById(R.id.searchEdit);
        mCountText = v.findViewById(R.id.countText);

        mSearchEdit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                mHandler.removeCallbacksAndMessages(null);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        searchNews(mSearchEdit.getText().toString());
                        if(keyCode == KeyEvent.KEYCODE_DEL)
                            searchNews(null);
                    }
                }, 300);
                return false;
            }
        });

        getPrograms();
        return v;
    }

    private void getPrograms(){
        ParseQuery<ParseObject> queryEvents = ParseQuery.getQuery("Program");
        queryEvents.include("faculty");
        queryEvents.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    news = new ArrayList<>();
                    filters = new ArrayList<>();
                    filters.add("Todos");
                    for (int i = 0; i < objects.size(); i++) {
                        ParseObject p = objects.get(i);
                        ParseObject faculty = p.getParseObject("faculty");
                        Program program = new Program(p.getString("name"), new Faculty(faculty.getString("name"), faculty.getObjectId()),
                                p.getString("description"), p.getString("study_plan"), p.getObjectId(), "");
                        if(p.getParseFile("image")!=null){
                            program.setImage(Objects.requireNonNull(p.getParseFile("image")).getUrl());
                        }
                        if(!isExistTag(faculty.getString("name"))){
                            filters.add(faculty.getString("name"));
                        }
                        news.add(program);
                    }
                    addFilters(filters);
                    confNews(news);
                    FiltersAdapter adapterFilter = new FiltersAdapter(getContext(), filters, (item, pos) -> {
                        if(pos != 0){
                            filterProm(item);
                        }else{
                            searchNews(null);
                        }
                    });
                    filterList.setAdapter(adapterFilter);
                }
            }
        });
    }

    private void searchNews(String search){
        if(search!=null){
            ArrayList<Program> newssearch = new ArrayList<>();
            for (Program prom: news) {
                if(prom.getName().toLowerCase().contains(search.toLowerCase()))
                    newssearch.add(prom);
            }
            confNews(newssearch);
        }else{
            confNews(news);
        }

    }

    private void confNews(ArrayList<Program> promotions){
        mCountText.setText(promotions.size()+" Programas");
        BasicAdapterPrograms adapter = new BasicAdapterPrograms(getContext(),promotions, (item, pos, pairs, image)->{
            Intent intent = new Intent(getActivity(), DescriptionActivity.class);
            intent.putExtra("title", item.getName());
            intent.putExtra("basicP", (Serializable) item);
            intent.putExtra("image", convert(image));
            intent.putExtra("date",  "");

            startActivity(intent);
        });
        promList.setAdapter(adapter);
    }

    private byte[] convert(ImageView imageView){
        try{
            if(imageView.getDrawable() != null){
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                return baos.toByteArray();
            }else
                return null;
        }catch (Exception e){
            return null;
        }
    }

    private boolean isExistTag(String name){
        for (String nameTag: filters) {
            if(nameTag.equals(name)) return true;
        }
        return false;
    }

    private void addFilters(ArrayList<String> filtersNew){
        if(filtersNew != null)
            for (String name: filtersNew) {
                if(!name.equals("") && !isExistTag(name)) filters.add(name);
            }
    }

    private void filterProm(String tag){
        ArrayList<Program> promotionsTag = new ArrayList<>();
        for (Program p: news) {
            if(isExistTag(tag)) promotionsTag.add(p);
        }
        confNews(promotionsTag);
    }

}
