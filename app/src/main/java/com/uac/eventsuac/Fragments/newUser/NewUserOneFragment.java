package com.uac.eventsuac.Fragments.newUser;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import com.google.android.material.textfield.TextInputEditText;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.uac.eventsuac.Models.Institution;
import com.uac.eventsuac.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewUserOneFragment extends Fragment {

    private TextInputEditText mFirstNameEdit, mLastNameEdit, mBithdateEdit, mAddressEdit, mPhoneEdit;
    private Spinner spinInstitution;
    private ArrayList<Institution> institutions;
    final Calendar myCalendar = Calendar.getInstance();

    public NewUserOneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_new_user_1, container, false);
        mFirstNameEdit = v.findViewById(R.id.firstNameEdit);
        mLastNameEdit = v.findViewById(R.id.lastNameEdit);
        mBithdateEdit = v.findViewById(R.id.bithdateEdit);
        mAddressEdit = v.findViewById(R.id.addressEdit);
        mPhoneEdit = v.findViewById(R.id.phoneEdit);
        spinInstitution = v.findViewById(R.id.institution);

        getDataIns();

        final DatePickerDialog.OnDateSetListener date = (view1, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        mBithdateEdit.setOnClickListener(view -> new DatePickerDialog(Objects.requireNonNull(getActivity()), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());
        return v;
    }

    @SuppressLint("SimpleDateFormat")
    public ParseObject[] getForm(ParseUser objectUser){
        if(inputChecks()){
            try {
            objectUser.put("firstName", mFirstNameEdit.getText().toString());
            objectUser.put("lastName", mLastNameEdit.getText().toString());
            objectUser.put("phone", mPhoneEdit.getText().toString());
            objectUser.put("address", mAddressEdit.getText().toString());
            objectUser.put("institution", ParseObject.createWithoutData("Institution",institutions.get(spinInstitution.getSelectedItemPosition() - 1).getObjectId()));
            objectUser.put("rols", Arrays.asList("3"));
            objectUser.put("year_birth", Objects.requireNonNull(new SimpleDateFormat("yyyy-MM-dd").parse(mBithdateEdit.getText().toString())));
            return new ParseObject[]{objectUser};
            } catch (java.text.ParseException e) {
                e.printStackTrace();
                return null;
            }
        }else{
            return null;
        }
    }

    private boolean inputChecks(){
        if(mFirstNameEdit.getText().toString().isEmpty()){
            mFirstNameEdit.setError("Campo requerido");
            return false;
        }
        if(mLastNameEdit.getText().toString().isEmpty()){
            mLastNameEdit.setError("Campo requerido");
            return false;
        }
        if(mBithdateEdit.getText().toString().isEmpty()){
            mBithdateEdit.setError("Campo requerido");
            return false;
        }
        if(mAddressEdit.getText().toString().isEmpty()){
            mAddressEdit.setError("Campo requerido");
            return false;
        }
        if(mPhoneEdit.getText().toString().isEmpty()){
            mPhoneEdit.setError("Campo requerido");
            return false;
        }
        if(spinInstitution.getSelectedItemPosition() == 0){
            Toast.makeText(getContext(), "Seleccione una institución", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";  //Formato del año
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        mBithdateEdit.setText(sdf.format(myCalendar.getTime()));
    }


    private String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch (IOException e) {
            return null;
        }
    }

    private boolean verifyNameLinea(String name, ArrayList<String> names){
        for (String s: names) {
            if(s.equals(name)) return true;
        }
        return false;
    }


    private String citySimplify(String name){
        String[] parts = name.split("\\(");
        return parts[0].toLowerCase();
    }

    private void getDataIns(){
        ParseQuery<ParseObject> queryInstitutions = ParseQuery.getQuery("Institution");
        queryInstitutions.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    institutions = new ArrayList<>();
                    ArrayList<String> names = new ArrayList<>();
                    names.add("Institución");
                    for (int i = 0; i < objects.size(); i++) {
                        ParseObject p = objects.get(i);
                        institutions.add(new Institution(p.getObjectId(), p.getString("name"), p.getString("address")));
                        names.add(p.getString("name"));
                    }
                    if(getActivity()!=null){
                        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), R.layout.content_spinner, names);
                        arrayAdapter.setDropDownViewResource(R.layout.content_select_spinner);
                        spinInstitution.setAdapter(arrayAdapter);
                    }
                }else{
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
}
