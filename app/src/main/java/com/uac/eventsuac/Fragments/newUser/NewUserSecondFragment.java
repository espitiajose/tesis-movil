package com.uac.eventsuac.Fragments.newUser;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.fragment.app.Fragment;
import com.parse.ParseUser;
import com.uac.eventsuac.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewUserSecondFragment extends Fragment {

    private CheckBox mCheckPolitics;

    public NewUserSecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_new_user_2, container, false);
        mCheckPolitics = v.findViewById(R.id.check_politics);
        return v;
    }


    public ParseUser getForm(ParseUser objectUser){
        if(inputChecks()){
            return objectUser;
        }else{
            return null;
        }
    }


    private boolean inputChecks() {
        if (!mCheckPolitics.isChecked()) {
            mCheckPolitics.setError("Campo requerido");
            return false;
        }
        return true;
    }

}
