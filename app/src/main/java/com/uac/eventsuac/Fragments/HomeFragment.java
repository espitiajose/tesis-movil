package com.uac.eventsuac.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.uac.eventsuac.Models.Faculty;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;
import com.uac.eventsuac.helpers.DashboardCommunication;
import com.uac.eventsuac.helpers.LocalData;

import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private ImageView mAuditImg;
    private DashboardCommunication dCommunication;
    private TextView mTitleVehicle;
    private LocalData localData;
    private Program programSelected;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        mAuditImg = v.findViewById(R.id.auditImg);
        localData = new LocalData(Objects.requireNonNull(getContext()));
        mTitleVehicle = v.findViewById(R.id.titleVehicle);
        getPrograms();

        mAuditImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dCommunication.changeView(2);
            }
        });

        v.findViewById(R.id.itemNew).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dCommunication.changeView(1);
            }
        });

        v.findViewById(R.id.itemProm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dCommunication.changeView(0);
            }
        });
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            dCommunication = (DashboardCommunication) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " Debe implementar la interfaz Actualizar en su Activity");
        }
    }

    private void getPrograms(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserProgram");
        query.include("program");
        query.getInBackground(localData.getVehicle(), new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null && object!= null) {
                    ParseObject pbj = object.getParseObject("program");
                    Program program = new Program(pbj.getString("name"),pbj.getString("description"),
                            pbj.getString("study_plan"), pbj.getObjectId(), object.getObjectId());
                    if(object.getParseFile("image")!= null) program.setImage(Objects.requireNonNull(pbj.getParseFile("image")).getUrl());
                    programSelected = program;
                    mTitleVehicle.setText(programSelected.getName());
                    Glide.with(getContext()).load(programSelected.getImage())
                            .error(getContext().getResources().getDrawable(R.drawable.background_notimage))
                            .placeholder(getContext().getResources().getDrawable(R.drawable.background_notimage))
                            .apply(RequestOptions.centerCropTransform()).into(mAuditImg);
                } else {
                    mTitleVehicle.setText("No tienes programa seleccionado");
                }
            }
        });
    }

}
