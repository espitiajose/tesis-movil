package com.uac.eventsuac.Fragments;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.uac.eventsuac.Adapters.CarsAdapter;
import com.uac.eventsuac.Models.Faculty;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;
import com.uac.eventsuac.helpers.DashboardCommunication;
import com.uac.eventsuac.helpers.LocalData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProgramsFragment extends Fragment {

    private RecyclerView mListCars;
    private Program programSelected;
    private String vehicleId;
    private LocalData localData;
    private TextView textRefVehicle;
    private EditText mSearchEdit;
    private Handler mHandler = new Handler();
    private DashboardCommunication dashboardCommunication;

    public MyProgramsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_programs, container, false);
        localData = new LocalData(Objects.requireNonNull(getContext()));
        textRefVehicle = v.findViewById(R.id.textRefVehicle);
        //mSearchEdit = v.findViewById(R.id.searchCarEdit);
        mListCars = v.findViewById(R.id.listCars);
        vehicleId = localData.getVehicle();
        setDataVehicle();
        getVehicles();

        /*
        mSearchEdit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                mHandler.removeCallbacksAndMessages(null);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getPromotions(mSearchEdit.getText().toString());
                        if(keyCode == KeyEvent.KEYCODE_DEL)
                            getPromotions(null);
                    }
                }, 300);
                return false;
            }
        });
        */

        v.findViewById(R.id.selectVehicleBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectVehicle();
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getVehicles();
    }

    private void getVehicles(){
        ParseQuery<ParseObject> queryVehicle = ParseQuery.getQuery("UserProgram");
        queryVehicle.whereEqualTo("user", ParseUser.getCurrentUser());
        queryVehicle.include("program");
        queryVehicle.include("program.faculty");
        queryVehicle.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    final ArrayList<Program> programs = new ArrayList<>();
                    for (ParseObject p: objects) {
                        ParseObject object = p.getParseObject("program");
                        ParseObject faculty = object.getParseObject("faculty");
                        Program program = new Program(object.getString("name"),object.getString("description"),
                                object.getString("study_plan"), object.getObjectId(), p.getObjectId());
                        if(object.getParseFile("image")!= null) program.setImage(Objects.requireNonNull(object.getParseFile("image")).getUrl());
                        programs.add(program);
                    }

                CarsAdapter carsAdapter = new CarsAdapter(getContext(), programs, ((item, pos) -> {
                    programSelected = item;
                }));
                mListCars.setAdapter(carsAdapter);
                if(programs.size() == 1){
                    programSelected = programs.get(0);
                    vehicleId = programs.get(0).getObjectId();
                    localData.setVehicle(programs.get(0).getObjectId());
                    carsAdapter.setSelectedPos(0);
                    setDataVehicle();
                }else{
                    for (int i=0;i<programs.size(); i++) {
                        Program v = programs.get(i);
                        if(vehicleId.equals(v.getObjectId())){
                            programSelected = v;
                            localData.setVehicle(v.getObjectId());
                            carsAdapter.setSelectedPos(i);
                            setDataVehicle();
                        }
                    }
                }

            }
            }
        });
    }

    void selectVehicle(){
        if(programSelected!=null){
            localData.setVehicle(programSelected.getObjectId());
            setDataVehicle();
            dashboardCommunication.setProgramSelected(programSelected);
        }
    }

    void setDataVehicle(){
        if(programSelected!=null){
            textRefVehicle.setText(programSelected.getName());
        }else{
            textRefVehicle.setText("Selecciona un programa");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            dashboardCommunication = (DashboardCommunication) context;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
