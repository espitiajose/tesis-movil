package com.uac.eventsuac.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.christophesmet.android.views.maskableframelayout.MaskableFrameLayout;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.uac.eventsuac.Fragments.EventsFragment;
import com.uac.eventsuac.Fragments.HomeFragment;
import com.uac.eventsuac.Fragments.MyProgramsFragment;
import com.uac.eventsuac.Fragments.ProfileFragment;
import com.uac.eventsuac.Fragments.ProgramFragment;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;
import com.uac.eventsuac.customViews.NotifyAlert;
import com.uac.eventsuac.helpers.DashboardCommunication;
import com.uac.eventsuac.helpers.LocalData;
import com.uac.eventsuac.helpers.OnSwipeTouchListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DashboardActivity extends AppCompatActivity implements DashboardCommunication {

    private ConstraintLayout content;
    private FrameLayout mContentFragments;
    private ImageButton mBtnOpenMenu;
    private boolean isOpen = false;
    private MaskableFrameLayout mMaskableFrameLayout;
    private TextView title, personName, vehicleName;
    private ImageView logo, imgprofile;
    private LocalData localData;
    private Program programSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        initMenu();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        localData = new LocalData(getApplicationContext());
        content = findViewById(R.id.content);
        vehicleName = findViewById(R.id.vehicleName);
        personName = findViewById(R.id.personName);
        mMaskableFrameLayout = findViewById(R.id.frm_mask_animated);
        mBtnOpenMenu = findViewById(R.id.btnOpenMenu);
        mContentFragments = findViewById(R.id.contentfragments);
        imgprofile = findViewById(R.id.imgprofile);
        title = findViewById(R.id.textTitle);
        logo = findViewById(R.id.logo);

        findViewById(R.id.swipeArea).setOnTouchListener(new OnSwipeTouchListener(DashboardActivity.this) {
            public void onSwipeRight() {
                contentAnimate();
            }
            public void onSwipeLeft() {
                if(isOpen) contentAnimate();
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            content.setBackground(getResources().getDrawable(R.drawable.background_gradient_blue));
        }


        mBtnOpenMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contentAnimate();
            }
        });

        findViewById(R.id.logOutBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ParseUser.logOut();
                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        findViewById(R.id.touchArea).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isOpen) contentAnimate();
            }
        });

        loadFragment(new HomeFragment(), false);
        setProgramSelected(null);
        getPrograms();
    }

    private void initMenu(){
        ArrayList<View> itemsMenu = new ArrayList<>();
        itemsMenu.add(findViewById(R.id.titleNews));
        itemsMenu.add(findViewById(R.id.titlePqrs));
        itemsMenu.add(findViewById(R.id.titleCar));
        itemsMenu.add(findViewById(R.id.titleProfile));
        itemsMenu.add(findViewById(R.id.titleHome));

        for (int i = 0; i<itemsMenu.size(); i++){
            final int index = i;
            itemsMenu.get(index).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectItemMenu(index, true);
                }
            });
        }
    }

    private void selectItemMenu(int i, boolean animate){
        switch (i){
            case 0:
                loadFragment(new EventsFragment(), false);
                title.setText("Eventos");
                break;
            case 1:
                loadFragment(new ProgramFragment(), false);
                title.setText("Programas");
                break;
            case 2:
                loadFragment(new MyProgramsFragment(), false);
                title.setText("Mis Programas");
                break;
            case 3:
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("dataProgram", vehicleName.getText().toString());
                profileFragment.setArguments(bundle);
                loadFragment(profileFragment, false);
                title.setText("Mi perfil");
                break;
            case 4:
                loadFragment(new HomeFragment(), false);
                title.setText("Inicio ");
                break;
        }
        logo.setVisibility(View.GONE);
        if(animate) contentAnimate();
    }

    private void loadFragment(Fragment fragment, Boolean addToBack){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(addToBack) transaction.addToBackStack(null);
        transaction.replace(R.id.contentfragments, fragment);
        transaction.commit();
    }

    private void contentAnimate(){
        ObjectAnimator animation;
        if(!isOpen){
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            float translationX = displayMetrics.widthPixels * 0.70f;
            animation = ObjectAnimator.ofPropertyValuesHolder(mMaskableFrameLayout,
                    PropertyValuesHolder.ofFloat("translationX", translationX),
                    PropertyValuesHolder.ofFloat("scaleX", 0.85f),
                    PropertyValuesHolder.ofFloat("scaleY", 0.85f));
            mMaskableFrameLayout.setMask(getResources().getDrawable(R.drawable.background_rounded_black));
            findViewById(R.id.touchArea).setVisibility(View.VISIBLE);
        }
        else{
            animation = ObjectAnimator.ofPropertyValuesHolder(mMaskableFrameLayout,
                    PropertyValuesHolder.ofFloat("translationX", 0f),
                    PropertyValuesHolder.ofFloat("scaleX", 1f),
                    PropertyValuesHolder.ofFloat("scaleY", 1f));
            mMaskableFrameLayout.setMask(getResources().getDrawable(R.drawable.background_rectangle_black));
            findViewById(R.id.touchArea).setVisibility(View.GONE);
        }
        animation.setDuration(500);
        animation.start();
        isOpen = !isOpen;
    }

    public void setProgramSelected(Program programSelected){
        this.programSelected = programSelected;
        ParseUser user = ParseUser.getCurrentUser();
        personName.setText(user.getString("firstName")+" "+user.getString("lastName"));
        ParseFile parseImg = user.getParseFile("image");
        if(parseImg != null)
            Glide.with(getApplicationContext()).load(parseImg.getUrl())
                    .placeholder(getResources().getDrawable(R.drawable.profile))
                    .error(getResources().getDrawable(R.drawable.profile))
                    .apply(RequestOptions.circleCropTransform()).into(imgprofile);
        if(programSelected!=null){
            vehicleName.setText(programSelected.getName());
        }
    }

    private void getPrograms(){
        ParseQuery<ParseObject> queryVehicle = ParseQuery.getQuery("UserProgram");
        queryVehicle.whereEqualTo("user", ParseUser.getCurrentUser());
        queryVehicle.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e==null){
                    final ArrayList<Program> programs = new ArrayList<>();
                    for (ParseObject p: objects) {
                        ParseQuery<ParseObject> queryProgram = ParseQuery.getQuery("program");
                        queryProgram.whereEqualTo("objectId", p.getString("program"));
                        queryProgram.getFirstInBackground(new GetCallback<ParseObject>() {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void done(ParseObject object, ParseException e) {
                                if(e == null){
                                    programs.add(new Program(object.getString("name"), Objects.requireNonNull(object.getParseFile("image")).getUrl(),
                                            object.getString("description"), object.getString("study_plan"), object.getObjectId()));
                                }
                            }
                        });
                    }


                    if(programs.size()==1){
                        programSelected = programs.get(0);
                        localData.setVehicle(programSelected.getObjectId());
                    }else{
                        if(programSelected!=null){
                            for (Program v: programs) {
                                if(programSelected.getObjectId().equals(v.getObjectId())){
                                    programSelected = programs.get(0);
                                    localData.setVehicle(programSelected.getObjectId());
                                }
                            }
                        }
                    }

                    setProgramSelected(programSelected);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void changeView(int index) {
        selectItemMenu(index, false);
    }

    @Override
    public Program getProgramSelected(){
        return programSelected;
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.contentfragments);
        if(isOpen){
            contentAnimate();
        } else if (count == 0 && currentFragment instanceof HomeFragment) {
            super.onBackPressed();
        } else if(count == 0) {
            loadFragment(new HomeFragment(), false);
        }else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
