package com.uac.eventsuac.Activities;

import android.app.Application;
import android.content.Intent;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.uac.eventsuac.R;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(
                new Parse.Configuration.Builder(this)
                        .applicationId(getString(R.string.back4app_app_id))
                        .clientKey(getString(R.string.back4app_client_key))
                        .server(getString(R.string.back4app_server_url))
                        .build()
        );
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.saveInBackground();
        ParseUser user = ParseUser.getCurrentUser();
        if(user != null) startActivity(new Intent(this, DashboardActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

}
