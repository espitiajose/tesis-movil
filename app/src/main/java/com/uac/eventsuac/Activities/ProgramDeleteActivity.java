package com.uac.eventsuac.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;

public class ProgramDeleteActivity extends AppCompatActivity {

    private EditText mOtherReasonEdit;
    private TextView nameVehicle, dataVehicle;
    private Program program;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_delete);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        nameVehicle = findViewById(R.id.nameVehicle);
        dataVehicle = findViewById(R.id.dataVehicle);
        mOtherReasonEdit = findViewById(R.id.otherReasonEdit);

        Bundle args = getIntent().getExtras();
        if(args!=null){
            program = (Program) args.getSerializable("program");
            int position = args.getInt("pos");
            nameVehicle.setText(program.getName());
            dataVehicle.setText("programa");
        }

        findViewById(R.id.deleteVehicleBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send();
            }
        });

        findViewById(R.id.btnback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    void send(){
        ParseObject objectVehicle = ParseObject.createWithoutData("UserProgram", program.getIdRef());
        objectVehicle.deleteInBackground();
        Toast.makeText(getApplicationContext(), "Vehiculo eliminado", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }
}
