package com.uac.eventsuac.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.uac.eventsuac.R;
import com.uac.eventsuac.customViews.NotifyAlert;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText mEmailEdit, mPasswordEdit;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        context = LoginActivity.this;

        mEmailEdit = findViewById(R.id.emailEdit);
        mPasswordEdit = findViewById(R.id.password);


        findViewById(R.id.btnregister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, NewUserActivity.class));
            }
        });

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    private void login(){
        if(inputsCheck()) {
            ParseUser.logInInBackground(mEmailEdit.getText().toString(), mPasswordEdit.getText().toString(), new LogInCallback() {
                public void done(ParseUser user, ParseException e) {
                    if (user != null) {
                        if(verifyRol(Objects.requireNonNull(user.getJSONArray("rols"))))
                            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                        else{
                            NotifyAlert notify = new NotifyAlert(context, true,
                                    user.get("firstName") +" "+ user.get("lastName"), "Error en inicio de sesión",
                                    "No tienes permiso para acceder a la plataforma movil", null,null);
                            notify.setCancelable(true);
                            notify.show();
                            cleanInputs();
                            ParseUser.logOut();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Usuario incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private boolean inputsCheck(){
        if(mEmailEdit.getText().toString().isEmpty()){
            mEmailEdit.setError("Campo requerido");
            return false;
        }
        if(mPasswordEdit.getText().toString().isEmpty()){
            mPasswordEdit.setError("Campo requerido");
            return false;
        }
        return true;
    }

    private boolean verifyRol(JSONArray rols){
        for (int i = 0; i < rols.length(); i++){
            try {
                if(rols.getString(i).equals("3")) return  true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void cleanInputs(){
        mEmailEdit.setText(null);
        mPasswordEdit.setText(null);
    }
}
