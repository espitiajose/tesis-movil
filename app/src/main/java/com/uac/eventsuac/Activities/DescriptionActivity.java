package com.uac.eventsuac.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.uac.eventsuac.Adapters.CarsAdapter;
import com.uac.eventsuac.Models.Event;
import com.uac.eventsuac.Models.Program;
import com.uac.eventsuac.R;
import com.uac.eventsuac.customViews.NotifyAlert;
import com.uac.eventsuac.helpers.Alarm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DescriptionActivity extends AppCompatActivity {


    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    private TextView mDescription, mTitle, mDate, mWorshopText, mLink, mSubtitleText, textTitle, linkTextTitle;
    private ImageView image;
    private Event basic;
    private Button mBtnSearchWorkshop;
    private ImageButton btnAction;
    private Button btnActionAsist;
    private String title;
    private boolean select_program;
    private boolean select_event;
    ParseObject pObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        mDescription = findViewById(R.id.descriptionText);
        mTitle = findViewById(R.id.titleText);
        mDate = findViewById(R.id.dateText);
        mWorshopText = findViewById(R.id.workshopText);
        mLink = findViewById(R.id.linkText);
        image = findViewById(R.id.imageProm);
        mSubtitleText = findViewById(R.id.subtitleText);
        mBtnSearchWorkshop = findViewById(R.id.btnSearchWorkshop);
        textTitle = findViewById(R.id.textTitle);
        linkTextTitle = findViewById(R.id.linkTextTitle);
        btnAction = findViewById(R.id.btnAction);
        btnActionAsist = findViewById(R.id.btnActionAsist);

        findViewById(R.id.btnback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openUrl();
            }
        });

        loadData();
    }

    private void loadData(){
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            title = bundle.getString("title");
            textTitle.setText(title);
            if(bundle.getSerializable("basic") != null){
                basic = (Event) bundle.getSerializable("basic");
                mTitle.setText(basic.getName());
                mDate.setText(bundle.getString("date"));
                mDescription.setText(basic.getDescription());
                byte[] bytes = bundle.getByteArray("image");
                if(bytes != null) Glide.with(getApplicationContext()).load(bytes).apply(RequestOptions.centerCropTransform()).into(image);
                else mLink.setVisibility(View.GONE);
                btnActionAsist.setVisibility(View.GONE);
                verifyEvent(basic);
            }else{
                Program basic = (Program) bundle.getSerializable("basicP");
                mTitle.setText(basic.getName());
                linkTextTitle.setVisibility(View.VISIBLE);
                mLink.setText(basic.getStudy_plan());
                mDate.setText(bundle.getString("date"));
                mDescription.setText(basic.getDescription());
                byte[] bytes = bundle.getByteArray("image");
                if(bytes != null) Glide.with(getApplicationContext()).load(bytes).apply(RequestOptions.centerCropTransform()).into(image);
                btnAction.setVisibility(View.GONE);
                verifyProgram(basic);
            }
        }
    }

    private void verifyEvent(Event event){
        ParseUser user = ParseUser.getCurrentUser();
        ParseQuery<ParseObject> queryVehicle = ParseQuery.getQuery("UserEvent");
        queryVehicle.whereEqualTo("user", user);
        queryVehicle.whereEqualTo("event", ParseObject.createWithoutData("Event", event.getObjectId()));
        queryVehicle.getFirstInBackground((object, e) -> {
            btnActionAsist.setVisibility(View.VISIBLE);
            if(object != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        btnActionAsist.setBackgroundColor(getColor(R.color.colorPrimaryDark));
                    }
                btnActionAsist.setText("No asistir");
                select_program = true;
                pObject = object;
            }else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    btnActionAsist.setBackgroundColor(getColor(R.color.colorAccent));
                }
                btnActionAsist.setText("Asistir");
                select_program = false;
                pObject = object;
            }

            btnActionAsist.setOnClickListener(view -> {
                if(!select_program){
                    ParseObject gameScore = new ParseObject("UserEvent");
                    gameScore.put("user", ParseUser.getCurrentUser());
                    gameScore.put("event", ParseObject.createWithoutData("Event", event.getObjectId()));
                    gameScore.saveInBackground();
                    select_program = true;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        btnActionAsist.setBackgroundColor(getColor(R.color.colorPrimaryDark));
                    }
                    btnActionAsist.setText("No asistir");
                    setAlarm(basic);
                    NotifyAlert notify = new NotifyAlert(this, true,
                            user.get("firstName") +" "+ user.get("lastName"), "Asistiras a un nuevo evento",
                            "Te recordaremos cuando esté proxima la fecha.", null,null);
                    notify.setCancelable(true);
                    notify.show();
                }else{
                    pObject.deleteInBackground();
                    select_program = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        btnActionAsist.setBackgroundColor(getColor(R.color.colorAccent));
                    }
                    btnActionAsist.setText("Asistir");
                }
            });
        });
    }

    private void verifyProgram(Program program){
        ParseUser user = ParseUser.getCurrentUser();
        ParseQuery<ParseObject> queryVehicle = ParseQuery.getQuery("UserProgram");
        queryVehicle.whereEqualTo("user", user);
        queryVehicle.whereEqualTo("program", ParseObject.createWithoutData("Program", program.getObjectId()));
        queryVehicle.getFirstInBackground((object, e) -> {
            btnAction.setVisibility(View.VISIBLE);
            if(object != null){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    btnAction.setBackground(getApplicationContext().getDrawable(android.R.drawable.star_on));
                }
                select_program = true;
                pObject = object;
            }else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    btnAction.setBackground(getApplicationContext().getDrawable(android.R.drawable.star_off));
                }
                select_program = false;
                pObject = object;
            }

            btnAction.setOnClickListener(view -> {
                if(!select_program){
                    ParseObject gameScore = new ParseObject("UserProgram");
                    gameScore.put("user", ParseUser.getCurrentUser());
                    gameScore.put("program", ParseObject.createWithoutData("Program", program.getObjectId()));
                    gameScore.saveInBackground();
                    select_program = true;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        btnAction.setBackground(getApplicationContext().getDrawable(android.R.drawable.star_on));
                    }
                    NotifyAlert notify = new NotifyAlert(this, true,
                            user.get("firstName") +" "+ user.get("lastName"), "Tienes un programa nuevo",
                            "Puedes verlo en la sesion 'mis programas'.", null,null);
                    notify.setCancelable(true);
                    notify.show();
                }else{
                    pObject.deleteInBackground();
                    select_program = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        btnAction.setBackground(getApplicationContext().getDrawable(android.R.drawable.star_off));
                    }
                }
            });
        });
    }


    private void loadFragment(Fragment fragment, Boolean addToBack){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(addToBack) transaction.addToBackStack(null);
        transaction.replace(R.id.contentDescription, fragment);
        transaction.commit();
    }





    @Override
    public void onBackPressed() {
        textTitle.setText(title);
        super.onBackPressed();
    }

    private void openUrl(){
        Uri uri = Uri.parse(mLink.getText().toString());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    private void setAlarm(Event event){
        // Alarma a las 8:30 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(event.getStartDate());
        Log.i("date1", calendar.toString());
        calendar.add(Calendar.HOUR, -23);
        calendar.add(Calendar.MINUTE, -58);
        Log.i("date2", calendar.toString());
        //long when = System.currentTimeMillis() + 60000L;
        long when = calendar.getTimeInMillis() + 120000L;
        AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, Alarm.class);
        intent.putExtra("myAction", "notify");
        intent.putExtra("event", event);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        assert am != null;
        am.set(AlarmManager.RTC_WAKEUP, when, pendingIntent);
    }
}
