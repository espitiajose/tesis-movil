package com.uac.eventsuac.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.uac.eventsuac.Fragments.newUser.NewUserOneFragment;
import com.uac.eventsuac.Fragments.newUser.NewUserSecondFragment;
import com.uac.eventsuac.Fragments.newUser.NewUserThreeFragment;
import com.uac.eventsuac.R;

public class NewUserActivity extends AppCompatActivity {

    private static NewUserOneFragment newUserOneFragment = new NewUserOneFragment();
    private static NewUserSecondFragment newUserSecondFragment = new NewUserSecondFragment();
    private static NewUserThreeFragment newUserThreeFragment = new NewUserThreeFragment();
    private ParseUser objectUser = new ParseUser();
    private Button btnNext;
    private int viewIndex = 0;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        progressDialog = new ProgressDialog(NewUserActivity.this);
        progressDialog.setTitle("Registrando nuevo usuario");
        progressDialog.setMessage("Esto puede tardar un momento...");
        btnNext = findViewById(R.id.btnNextUser);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionNext();
            }
        });
        findViewById(R.id.btnback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        nextView();
    }

    private void nextView(){
        switch (viewIndex){
            case 0:
                btnNext.setText("Siguiente");
                loadFragment(newUserOneFragment, false);
                break;
            case 1:
                btnNext.setText("Siguiente");
                loadFragment(newUserSecondFragment, true);
                break;
            case 2:
                btnNext.setText("Registrar");
                loadFragment(newUserThreeFragment, true);
                break;
            case 3:
                progressDialog.show();
                objectUser.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e == null){
                            ParseUser.logOut();
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Usuario registrado!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(  NewUserActivity.this, LoginActivity.class));
                            finish();
                        }else{
                            e.printStackTrace();
                        }
                    }
                });
                break;
        }
    }



    private void loadFragment(Fragment fragment, Boolean addToBack){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(addToBack) transaction.addToBackStack(null);
        transaction.replace(R.id.contentFragmentsRegister, fragment);
        transaction.commit();
    }

    private void actionNext(){
        ParseUser objectForm = null;
        if(viewIndex == 0){
            ParseObject[] arrayObjects = newUserOneFragment.getForm(objectUser);
            if(arrayObjects!=null){
                objectForm = (ParseUser) arrayObjects[0];
            }
        }
        if(viewIndex == 1){
            objectForm = newUserSecondFragment.getForm(objectUser);
        }
        if(viewIndex == 2){
            objectForm = newUserThreeFragment.getForm(objectUser);
        }
        if(objectForm != null){
            objectUser = objectForm;
            viewIndex++;
            nextView();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        viewIndex--;
    }
}
