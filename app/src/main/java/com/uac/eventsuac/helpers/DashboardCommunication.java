package com.uac.eventsuac.helpers;

import com.uac.eventsuac.Models.Program;

public interface DashboardCommunication {

    void changeView(int index);

    void setProgramSelected(Program program);

    Program getProgramSelected();
}
