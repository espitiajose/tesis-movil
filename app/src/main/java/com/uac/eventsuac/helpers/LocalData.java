package com.uac.eventsuac.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalData {

        private static final String APP_SHARED_PREFS = "MyPrefs";

        private SharedPreferences appSharedPrefs;
        private SharedPreferences.Editor prefsEditor;

        private static final String currentSession="currentSession";
        private static final String currentRead="currentRead";
        private static final String currentVehicle="currentVehicle";


        public LocalData(Context context)
        {
            this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
            this.prefsEditor = appSharedPrefs.edit();
        }

        public void reset()
        {
            prefsEditor.remove(currentSession);
            prefsEditor.commit();

        }

    public String getVehicle()
    {
        return appSharedPrefs.getString(currentVehicle, null);
    }

    public void setVehicle(String vehicleId)
    {
        prefsEditor.putString(currentVehicle, vehicleId);
        prefsEditor.commit();
    }
}
