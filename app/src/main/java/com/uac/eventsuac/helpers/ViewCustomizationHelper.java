package com.uac.eventsuac.helpers;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class ViewCustomizationHelper {

    public static final String BOLD_SOURCE = "font/ralewayextrabold.ttf";
    public static final String SEMIBOLD_SOURCE = "font/ralewaysemibold.ttf";
    public static final String REGULAR_SOURCE = "font/ralewaybold.ttf";

    public static void setFont(View v, String source, Context context)
    {
        ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), source));
    }


    public static void changeGradientBackground(View v, int color, int radius)
    {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(radius);

        if(v instanceof EditText || v instanceof ViewGroup) {
            shape.setStroke(1, color);
            shape.setColor(Color.parseColor("#FFFFFF"));
        }
        else
            shape.setColor(color);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackground(shape);
        }
    }

    public static void changeGradientBackground(View v, int color, float[] radii)
    {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadii(radii);
        shape.setColor(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackground(shape);
        }
    }
}
