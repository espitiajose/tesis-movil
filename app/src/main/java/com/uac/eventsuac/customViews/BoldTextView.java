package com.uac.eventsuac.customViews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;
import com.uac.eventsuac.helpers.ViewCustomizationHelper;


public class BoldTextView extends AppCompatTextView {


    public BoldTextView(Context context) {
        super(context);
        ViewCustomizationHelper.setFont(this, ViewCustomizationHelper.BOLD_SOURCE, context);
    }

    public BoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ViewCustomizationHelper.setFont(this, ViewCustomizationHelper.BOLD_SOURCE, context);

    }

    public BoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ViewCustomizationHelper.setFont(this, ViewCustomizationHelper.BOLD_SOURCE, context);
    }

}
