package com.uac.eventsuac.customViews;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.uac.eventsuac.R;

public class NotifyAlert extends AlertDialog {

    public NotifyAlert(Context context, boolean cancelable, String upTitle, String upSubtitle, String title, String subtitle, ActionOnItemClickListener actionlistiner) {
        super(context);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.layout_alert_dialog, null);
        setCancelable(cancelable);
        setCanceledOnTouchOutside(cancelable);
        TextView mUpTitle = v.findViewById(R.id.upTitle);
        mUpTitle.setText(upTitle);
        TextView mUpSubTitle = v.findViewById(R.id.upSubject);
        mUpSubTitle.setText(upSubtitle);
        TextView mTitle = v.findViewById(R.id.titleAlert);
        mTitle.setText(title);
        TextView mSubTitle = v.findViewById(R.id.subtitleAlert);
        mSubTitle.setText(subtitle);
        ImageView imgNotify = v.findViewById(R.id.clientImg);
        Button mOkBtn = v.findViewById(R.id.positive_btn);
        Button mNotBtn = v.findViewById(R.id.negative_btn);
        setCancelable(false);
        setView(v);
        NotifyAlert dialog = this;
        if(actionlistiner != null){
            v.findViewById(R.id.contentButtons).setVisibility(View.VISIBLE);
            mOkBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionlistiner.onItemClick(dialog, true);
                    dismiss();
                }
            });
            mNotBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionlistiner.onItemClick(dialog, false);
                    dismiss();
                }
            });
        }
    }

    public interface ActionOnItemClickListener {
        void onItemClick(NotifyAlert dialog, boolean response);
    }
}
