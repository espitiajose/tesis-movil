package com.uac.eventsuac.customViews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;
import com.uac.eventsuac.helpers.ViewCustomizationHelper;

public class RegularTextView extends AppCompatTextView {

    public RegularTextView(Context context) {
        super(context);
        ViewCustomizationHelper.setFont(this, ViewCustomizationHelper.REGULAR_SOURCE, context);
    }

    public RegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ViewCustomizationHelper.setFont(this, ViewCustomizationHelper.REGULAR_SOURCE, context);

    }

    public RegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ViewCustomizationHelper.setFont(this, ViewCustomizationHelper.REGULAR_SOURCE, context);
    }

}
