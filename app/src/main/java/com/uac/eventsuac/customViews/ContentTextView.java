package com.uac.eventsuac.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;
import com.uac.eventsuac.helpers.FontCache;
import com.uac.eventsuac.helpers.ViewCustomizationHelper;

public class ContentTextView extends AppCompatTextView {
    public ContentTextView(Context context) {
        super(context);
    }

    public ContentTextView(Context context, AttributeSet attrs) {

        super(context, attrs);
        applyCustomFont(context);
    }

    public ContentTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface(ViewCustomizationHelper.SEMIBOLD_SOURCE, context);
        setTypeface(customFont);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        String newText="";
        if(text != null)
            newText = stripHtml(text.toString());
        super.setText(newText, type);


    }

    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }
}
