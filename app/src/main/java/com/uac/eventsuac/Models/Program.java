package com.uac.eventsuac.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Program implements Serializable  {

    private String name;
    private String image;
    private String description;
    private String study_plan;
    private String objectId;
    private String idRef;
    private Faculty faculty;

    public Program(String name, String image, String description, String study_plan, String objectId) {
        this.name = name;
        this.image = image;
        this.description = description;
        this.study_plan = study_plan;
        this.objectId = objectId;
    }

    public Program(String name, String description, String study_plan, String objectId) {
        this.name = name;
        this.description = description;
        this.study_plan = study_plan;
        this.objectId = objectId;
    }

    public Program(String name, Faculty faculty, String description, String study_plan, String objectId, String idRef) {
        this.name = name;
        this.faculty = faculty;
        this.description = description;
        this.study_plan = study_plan;
        this.objectId = objectId;
    }

    public Program(String name) {
        this.name = name;
    }

    public Program(String name, String image, String description, String study_plan, String objectId, String idRef) {
        this.name = name;
        this.image = image;
        this.description = description;
        this.study_plan = study_plan;
        this.objectId = objectId;
        this.idRef = idRef;
    }

    protected Program(Parcel in) {
        name = in.readString();
        image = in.readString();
        description = in.readString();
        study_plan = in.readString();
        objectId = in.readString();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStudy_plan() {
        return study_plan;
    }

    public void setStudy_plan(String study_plan) {
        this.study_plan = study_plan;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getIdRef() {
        return idRef;
    }

    public void setIdRef(String idRef) {
        this.idRef = idRef;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }





}
