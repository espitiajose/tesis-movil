package com.uac.eventsuac.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {

    private String objectId;
    private String place;
    private Date startDate;
    private Date endDate;
    private String name;
    private boolean approved;
    private String description;
    private String image;
    private Program program;

    public Event(String objectId, String place, Date startDate, Date endDate, String name, boolean approved, String description) {
        this.objectId = objectId;
        this.place = place;
        this.startDate = startDate;
        this.endDate = endDate;
        this.name = name;
        this.approved = approved;
        this.description = description;
    }

    public Event(String objectId, String place, Date startDate, Date endDate, String name, boolean approved, String description, String image) {
        this.objectId = objectId;
        this.place = place;
        this.startDate = startDate;
        this.endDate = endDate;
        this.name = name;
        this.approved = approved;
        this.description = description;
        this.image = image;
    }

    protected Event(Parcel in) {
        objectId = in.readString();
        place = in.readString();
        name = in.readString();
        approved = in.readByte() != 0;
        description = in.readString();
        image = in.readString();
        program = in.readParcelable(Program.class.getClassLoader());
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
}
