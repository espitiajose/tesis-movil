package com.uac.eventsuac.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Institution implements Serializable, Parcelable {

    private String objectId;
    private String name;
    private String address;

    public Institution(String objectId, String name, String address) {
        this.objectId = objectId;
        this.name = name;
        this.address = address;
    }

    protected Institution(Parcel in) {
        objectId = in.readString();
        name = in.readString();
        address = in.readString();
    }

    public static final Creator<Institution> CREATOR = new Creator<Institution>() {
        @Override
        public Institution createFromParcel(Parcel in) {
            return new Institution(in);
        }

        @Override
        public Institution[] newArray(int size) {
            return new Institution[size];
        }
    };

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(objectId);
        dest.writeString(name);
        dest.writeString(address);
    }
}
