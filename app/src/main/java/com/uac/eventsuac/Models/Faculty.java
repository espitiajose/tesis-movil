package com.uac.eventsuac.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Faculty implements Serializable, Parcelable {

    private String name;
    private String image;
    private String objectId;

    public Faculty(String name, String image, String objectId) {
        this.name = name;
        this.image = image;
        this.objectId = objectId;
    }

    public Faculty(String name, String objectId) {
        this.name = name;
        this.objectId = objectId;
    }

    protected Faculty(Parcel in) {
        name = in.readString();
        image = in.readString();
        objectId = in.readString();
    }

    public static final Creator<Faculty> CREATOR = new Creator<Faculty>() {
        @Override
        public Faculty createFromParcel(Parcel in) {
            return new Faculty(in);
        }

        @Override
        public Faculty[] newArray(int size) {
            return new Faculty[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(objectId);
    }
}
